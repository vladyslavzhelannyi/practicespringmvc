package com.example.springmvc.constant;

public class CrudExceptionConstant {
    public static final String MSG_NO_SUCH_USER = "No user with such id";
    public static final String MSG_REPEATING_ID = "User with such id already exists";
}
