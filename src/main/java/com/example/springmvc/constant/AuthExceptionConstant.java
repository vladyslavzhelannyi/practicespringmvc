package com.example.springmvc.constant;

public class AuthExceptionConstant {
    public static final String MSG_WRONG_CREDENTIALS = "Login or password is invalid";
}
