package com.example.springmvc.constant;

public class KeyExceptionConstant {
    public static final String MSG_NO_KEY = "No key in headers";
    public static final String MSG_INVALID_KEY = "Key is invalid or outdated";
}
