package com.example.springmvc.util;

import com.google.common.hash.Hashing;
import org.springframework.stereotype.Component;
import java.nio.charset.StandardCharsets;

@Component
public class KeyGenerator {
    public String generateKey(String login, String password, String salt) {
        String stringToHash = login.concat(password).concat(salt);
        String sha256hex = Hashing.sha256()
                .hashString(stringToHash, StandardCharsets.UTF_8)
                .toString();
        return sha256hex;
    }
}
