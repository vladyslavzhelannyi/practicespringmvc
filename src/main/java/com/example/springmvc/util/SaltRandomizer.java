package com.example.springmvc.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.Random;

@Component
public class SaltRandomizer {
    private Random random;

    @Autowired
    public SaltRandomizer(Random random) {
        this.random = random;
    }

    public String buildSalt(int saltLength, char[] chars) {
        StringBuilder salt = new StringBuilder();
        for (int i = 0; i < saltLength; i++) {
            char nextChar = chars[random.nextInt(chars.length)];
            salt.append(nextChar);
        }
        return salt.toString();
    }
}
