package com.example.springmvc.exception;

public class AuthException extends Exception {
    public AuthException(String message) {
        super(message);
    }
}
