package com.example.springmvc.exception;

public class UserCacheException extends Exception {
    public UserCacheException(String message) {
        super(message);
    }
}
