package com.example.springmvc.exception;

public class KeyException extends Exception {
    public KeyException(String message) {
        super(message);
    }
}
