package com.example.springmvc.config;

import com.example.springmvc.filter.KeyFilter;
import com.example.springmvc.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.example.springmvc.service")
public class FilterConfig {
    private final AuthService authService;

    @Autowired
    public FilterConfig(AuthService authService) {
        this.authService = authService;
    }

    @Bean(name = "keyFilter")
    public FilterRegistrationBean<KeyFilter> keyFilter() {
        FilterRegistrationBean<KeyFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new KeyFilter(authService));
        registrationBean.addUrlPatterns("/users/*");
        return registrationBean;
    }
}
