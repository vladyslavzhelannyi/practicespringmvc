package com.example.springmvc.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.Random;

@Configuration
public class AppConfig {
    @Bean(name = "random")
    public Random getRandom() {
        return new Random();
    }
}
