package com.example.springmvc.controller;

import com.example.springmvc.exception.UserCacheException;
import com.example.springmvc.model.User;
import com.example.springmvc.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
public class CrudController {
    private final CrudService crudService;

    @Autowired
    public CrudController(CrudService crudService) {
        this.crudService = crudService;
    }

    @GetMapping("/users/getall")
    public List<Object> getUserList() {
        return crudService.getUsersList();
    }

    @PostMapping("/users/add")
    public void addUser(@RequestBody @Valid User user) throws UserCacheException {
        crudService.addUser(user);
    }

    @GetMapping("/users/get")
    public User getUser(@RequestParam(name = "id") int id) throws UserCacheException {
        return crudService.getUser(id);
    }

    @PostMapping("/users/update")
    public void updateUser(@RequestBody @Valid User user) throws UserCacheException {
        crudService.updateUser(user);
    }

    @DeleteMapping("/users/delete")
    public void deleteUser(@RequestParam(name = "id") int id) throws UserCacheException {
        crudService.deleteUser(id);
    }
}
