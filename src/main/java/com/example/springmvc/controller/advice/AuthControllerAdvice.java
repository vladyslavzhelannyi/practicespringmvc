package com.example.springmvc.controller.advice;

import com.example.springmvc.exception.AuthException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import javax.servlet.http.HttpServletRequest;

@ControllerAdvice(basePackageClasses = AuthControllerAdvice.class)
public class AuthControllerAdvice {
    @ExceptionHandler(value = AuthException.class)
    public ResponseEntity<String> handleAuthException(HttpServletRequest request, Exception ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }
}
