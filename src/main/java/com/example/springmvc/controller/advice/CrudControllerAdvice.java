package com.example.springmvc.controller.advice;

import com.example.springmvc.exception.KeyException;
import com.example.springmvc.exception.UserCacheException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import javax.servlet.http.HttpServletRequest;

@ControllerAdvice(basePackageClasses = CrudControllerAdvice.class)
public class CrudControllerAdvice {
    @ExceptionHandler(value = UserCacheException.class)
    public ResponseEntity<String> handleUserCacheException(HttpServletRequest request, Exception ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = KeyException.class)
    public ResponseEntity<String> handleKeyException(HttpServletRequest request, Exception ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.UNAUTHORIZED);
    }
}
