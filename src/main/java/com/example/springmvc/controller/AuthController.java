package com.example.springmvc.controller;

import com.example.springmvc.dto.AuthDto;
import com.example.springmvc.exception.AuthException;
import com.example.springmvc.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;

@RestController
public class AuthController {
    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping(value = "/auth")
    public String authorize(@RequestBody @Valid AuthDto authDto) throws AuthException {
        return authService.authorize(authDto);
    }
}
