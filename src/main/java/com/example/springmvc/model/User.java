package com.example.springmvc.model;

import lombok.*;
import javax.validation.constraints.*;

@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
@NotNull
public class User {
    @Positive
    private final int id;
    @Pattern(regexp = "[A-Z][a-z]+", message = "Wrong pattern")
    private String name;
    @Pattern(regexp = "[A-Z][a-z]+", message = "Wrong pattern")
    private String surname;
    @Min(0)
    @Max(150)
    private int age;
}
