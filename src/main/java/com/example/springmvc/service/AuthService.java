package com.example.springmvc.service;

import com.example.springmvc.dto.AuthDto;
import com.example.springmvc.exception.AuthException;
import com.example.springmvc.util.KeyGenerator;
import com.example.springmvc.worker.SaltChanger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import static com.example.springmvc.constant.AuthExceptionConstant.MSG_WRONG_CREDENTIALS;

@Component
@ComponentScan(basePackageClasses = SaltChanger.class)
public class AuthService {
    private final SaltChanger saltChanger;
    private final KeyGenerator keyGenerator;
    @Value("${login}")
    private String login;
    @Value("${password}")
    private String password;

    @Autowired
    public AuthService(SaltChanger saltChanger, KeyGenerator keyGenerator) {
        this.saltChanger = saltChanger;
        this.keyGenerator = keyGenerator;
    }

    public String authorize(AuthDto authDto) throws AuthException {
        if (!authDto.getLogin().equals(login) || !authDto.getPassword().equals(password)) {
            throw new AuthException(MSG_WRONG_CREDENTIALS);
        }
        return keyGenerator.generateKey(authDto.getLogin(), authDto.getPassword(), saltChanger.getSalt());
    }

    public String getDefaultKey() {
        return keyGenerator.generateKey(login, password, saltChanger.getSalt());
    }
}
