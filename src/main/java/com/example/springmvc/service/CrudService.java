package com.example.springmvc.service;

import com.example.springmvc.cache.UserCache;
import com.example.springmvc.exception.UserCacheException;
import com.example.springmvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrudService {
    private final UserCache userCache;

    @Autowired
    public CrudService(UserCache userCache) {
        this.userCache = userCache;
    }

    public List<Object> getUsersList() {
        return List.of(userCache.getAll());
    }

    public void addUser(User user) throws UserCacheException {
        userCache.add(user);
    }

    public User getUser(int id) throws UserCacheException {
        return userCache.get(id);
    }

    public void updateUser(User user) throws UserCacheException {
        userCache.update(user);
    }

    public void deleteUser(int id) throws UserCacheException {
        userCache.delete(id);
    }
}
