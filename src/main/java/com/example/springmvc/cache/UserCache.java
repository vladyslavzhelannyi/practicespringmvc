package com.example.springmvc.cache;

import com.example.springmvc.exception.UserCacheException;
import com.example.springmvc.model.User;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static com.example.springmvc.constant.CrudExceptionConstant.*;

@Component
public class UserCache {
    private final List<User> cache = Collections.synchronizedList(new ArrayList<>());

    public synchronized boolean add(User user) throws UserCacheException {
        if (contain(user.getId())) {
            throw new UserCacheException(MSG_REPEATING_ID);
        }
        cache.add(user);
        System.out.println(cache);
        return true;
    }

    public synchronized boolean update(User user) throws UserCacheException {
        int id = user.getId();
        for (User nextUser : cache) {
            if (nextUser.getId() == id) {
//                nextUser.setName(user.getName());
//                nextUser.setSurname(user.getSurname());
//                nextUser.setAge(user.getAge());
                cache.remove(nextUser); //можно ли так писать?
                cache.add(user);
                System.out.println(cache);
                return true;
            }
        }
        throw new UserCacheException(MSG_NO_SUCH_USER);
    }

    public synchronized boolean delete(int id) throws UserCacheException {
        for (User user : cache) {
            if (user.getId() == id) {
                cache.remove(user);
                System.out.println(cache);
                return true;
            }
        }
        throw new UserCacheException(MSG_NO_SUCH_USER);
    }

    public User get(int id) throws UserCacheException {
        return cache.stream().filter(user -> user.getId() == id).findFirst().orElseThrow(() -> new UserCacheException(MSG_NO_SUCH_USER));
    }

    public boolean contain(int id) { //нужно ли синхронизировать?
        return cache.stream().anyMatch(user -> user.getId() == id);
    }

    public Object[] getAll() {
        return cache.toArray();
    }
}
