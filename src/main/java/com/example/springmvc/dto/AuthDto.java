package com.example.springmvc.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import javax.validation.constraints.NotEmpty;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class AuthDto {
    @NotEmpty
    private final String login;
    @NotEmpty
    private final String password;
}
