package com.example.springmvc.filter;

import com.example.springmvc.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import static com.example.springmvc.constant.AuthorizationConstant.HEADER_KEY;
import static com.example.springmvc.constant.KeyExceptionConstant.MSG_INVALID_KEY;
import static com.example.springmvc.constant.KeyExceptionConstant.MSG_NO_KEY;

public class KeyFilter implements Filter {
    private final AuthService authService;

    public KeyFilter(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
        String headerKey = httpServletRequest.getHeader(HEADER_KEY);
        if (headerKey == null) {
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            httpServletResponse.getWriter().println(MSG_NO_KEY);
            return;
        }
        if (!authService.getDefaultKey().equals(headerKey)) {
            httpServletResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            httpServletResponse.getWriter().println(MSG_INVALID_KEY);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
