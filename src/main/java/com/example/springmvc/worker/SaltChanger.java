package com.example.springmvc.worker;

import com.example.springmvc.util.SaltRandomizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import static com.example.springmvc.constant.SaltRandomizerConstant.SALT_CHARS;
import static com.example.springmvc.constant.SaltRandomizerConstant.SALT_SIZE;

@Service
@ComponentScan(basePackageClasses = SaltRandomizer.class)
public class SaltChanger {
    private final SaltRandomizer saltRandomizer;
    private String salt;

    @Autowired
    public SaltChanger(SaltRandomizer saltRandomizer) {
        this.saltRandomizer = saltRandomizer;
    }

    @Scheduled(fixedRate = 180_000)
    public void run() {
        changeSalt();
    }

    public void changeSalt() {
        salt = saltRandomizer.buildSalt(SALT_SIZE, SALT_CHARS);
        System.out.println(salt);
    }

    public String getSalt() {
        return salt;
    }
}
